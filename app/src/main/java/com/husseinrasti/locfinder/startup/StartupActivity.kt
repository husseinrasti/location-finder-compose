package com.husseinrasti.locfinder.startup

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.husseinrasti.locfinder.databinding.ActivityStartupBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class StartupActivity : AppCompatActivity() {

    private lateinit var binding: ActivityStartupBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityStartupBinding.inflate(layoutInflater)
        setContentView(binding.root)
    }

}