package com.husseinrasti.components

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ProgressBar
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter
import com.husseinrasti.components.databinding.LoadStateItemBinding
import timber.log.Timber

class FooterStateAdapter(
    private val retry: () -> Unit
) : LoadStateAdapter<FooterStateAdapter.LoadingStateViewHolder>() {

    override fun onBindViewHolder(holder: LoadingStateViewHolder, loadState: LoadState) {
        holder.bind(loadState)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        loadState: LoadState
    ): LoadingStateViewHolder = LoadingStateViewHolder(parent)

    inner class LoadingStateViewHolder(
        private val parent: ViewGroup
    ) : RecyclerView.ViewHolder(
        LayoutInflater.from(parent.context)
            .inflate(R.layout.load_state_item, parent, false)
    ) {
        private val binding = LoadStateItemBinding.bind(itemView)
        private val progressBar: ProgressBar = binding.progressBar
        private val errorMsg: AppCompatTextView = binding.error
        private val retry: AppCompatButton = binding.retry
            .also { it.setOnClickListener { retry() } }

        fun bind(loadState: LoadState) {
            Timber.i("FooterStateAdapter: $loadState")
            Timber.i("FooterStateAdapter end: ${loadState.endOfPaginationReached}")
            progressBar.visibility(loadState is LoadState.Loading)
            retry.visibility(loadState is LoadState.Error)
            errorMsg.visibility(
                loadState is LoadState.Error || loadState.endOfPaginationReached
            )
            if (loadState is LoadState.Error) {
                errorMsg.text = loadState.error.localizedMessage
            } else if (loadState.endOfPaginationReached) {
                errorMsg.text = parent.context.getString(R.string.msg_filled)
            }
        }
    }

}