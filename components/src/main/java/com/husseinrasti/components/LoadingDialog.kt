package com.husseinrasti.components

import android.app.Dialog
import android.graphics.drawable.InsetDrawable
import android.graphics.drawable.NinePatchDrawable
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.FragmentManager

class LoadingDialog : DialogFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        isCancelable = false

        // Remove BG of the Dialog
        dialog?.window?.decorView?.background?.let {
            if (it is InsetDrawable) {
                it.alpha = 0
            } else if (it is NinePatchDrawable) {
                it.alpha = 0
            }
        }

        return inflater.inflate(R.layout.loading_dialog, container, false)
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        try {
            dialog.window!!.requestFeature(Window.FEATURE_NO_TITLE)
        } catch (e: Exception) {
            e.printStackTrace()
        }
        return dialog
    }

    override fun onResume() {
        super.onResume()
        try {
            dialog!!.window!!.setLayout(resources.getDimensionPixelSize(R.dimen._90dp), resources.getDimensionPixelSize(R.dimen._90dp))
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    fun show(manager: FragmentManager) {
        super.show(manager, javaClass.simpleName)
    }

}