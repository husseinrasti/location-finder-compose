package com.husseinrasti.nearby_places.data.entity.remote

import com.google.gson.annotations.SerializedName

data class ResultResponse(
    @SerializedName("results")
    val results: List<PlaceResponse>?,
    @SerializedName("summary")
    val summary: SummaryResponse?
)