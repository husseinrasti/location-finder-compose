package com.husseinrasti.nearby_places.data.dao

import androidx.paging.PagingSource
import androidx.room.*
import com.husseinrasti.nearby_places.data.entity.dto.PlaceEntity


/**
 * Created by Hussein Rasti.
 */
@Dao
interface PlaceDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(list: List<PlaceEntity.Item>)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun insert(entity: PlaceEntity.Item)

    @Update(onConflict = OnConflictStrategy.REPLACE)
    suspend fun update(entity: PlaceEntity.Item)

    @Delete
    suspend fun delete(entity: PlaceEntity.Item)

    @Query("SELECT * FROM tbl_places")
    fun select(): PagingSource<Int, PlaceEntity.Item>

    @Query("SELECT COUNT(id) FROM tbl_places")
    fun count(): Int

    @Query("DELETE FROM tbl_places")
    suspend fun clear()

}