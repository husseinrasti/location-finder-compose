package com.husseinrasti.nearby_places.ui

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.husseinrasti.components.visibility
import com.husseinrasti.components.R
import com.husseinrasti.nearby_places.data.entity.dto.PlaceEntity
import com.husseinrasti.nearby_places.databinding.PlacesAdapterBinding


/**
 * Created by Hussein Rasti on 3/21/22.
 */
class PlaceAdapter(
    private val onClickItem: (String) -> Unit
) : PagingDataAdapter<PlaceEntity.Item, PlaceAdapter.ViewHolder>(DiffCallBack()) {

    private lateinit var context: Context

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        getItem(position)?.let { item ->
            holder.bind(item)
            holder.itemView.setOnClickListener { onClickItem(item.id) }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        context = parent.context
        return ViewHolder(
            PlacesAdapterBinding.inflate(LayoutInflater.from(context), parent, false)
        )
    }


    inner class ViewHolder(
        private val binding: PlacesAdapterBinding
    ) : RecyclerView.ViewHolder(binding.root) {
        fun bind(item: PlaceEntity.Item) {
            binding.name.text = item.name
            binding.address.text = item.address
            binding.phone.visibility(item.phone.isNotEmpty())
            binding.phone.text = context.getString(R.string.str_phone, item.phone)
            binding.page.visibility(item.url.isNotEmpty())
            binding.page.text = context.getString(R.string.str_website, item.url)
        }
    }


    private class DiffCallBack : DiffUtil.ItemCallback<PlaceEntity.Item>() {
        override fun areItemsTheSame(oldItem: PlaceEntity.Item, newItem: PlaceEntity.Item): Boolean {
            return oldItem == newItem
        }

        override fun areContentsTheSame(oldItem: PlaceEntity.Item, newItem: PlaceEntity.Item): Boolean {
            return oldItem == newItem
        }
    }

}