package com.husseinrasti.nearby_places.di

import com.husseinrasti.nearby_places.data.dao.PlaceDao
import com.husseinrasti.nearby_places.data.datasource.NearbyPlaceRemoteMediator
import com.husseinrasti.nearby_places.data.repository.PlaceRepository
import com.husseinrasti.nearby_places.data.repository.PlaceRepositoryImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped


/**
 * Created by Hussein Rasti on 5/27/22.
 */

@Module
@InstallIn(ViewModelComponent::class)
class RepositoryModule {

    @Provides
    @ViewModelScoped
    fun providePlaceRepository(
        nearbyPlaceRemoteMediator: NearbyPlaceRemoteMediator,
        dao: PlaceDao
    ): PlaceRepository = PlaceRepositoryImpl(
        dao = dao,
        nearbyPlaceRemoteMediator = nearbyPlaceRemoteMediator
    )

}