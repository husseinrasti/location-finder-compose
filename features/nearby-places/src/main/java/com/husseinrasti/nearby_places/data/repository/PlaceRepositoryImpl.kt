package com.husseinrasti.nearby_places.data.repository

import androidx.paging.ExperimentalPagingApi
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import com.husseinrasti.core.extensions.allowReads
import com.husseinrasti.core.model.NETWORK_PAGE_SIZE
import com.husseinrasti.nearby_places.data.dao.PlaceDao
import com.husseinrasti.nearby_places.data.datasource.NearbyPlaceRemoteMediator
import com.husseinrasti.nearby_places.data.entity.dto.PlaceEntity
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject


/**
 * Created by Hussein Rasti.
 */
class PlaceRepositoryImpl @Inject constructor(
    private val nearbyPlaceRemoteMediator: NearbyPlaceRemoteMediator,
    private val dao: PlaceDao
) : PlaceRepository {

    override fun getNearbyPlaces(latitude: Double, longitude: Double): Flow<PagingData<PlaceEntity.Item>> {
        return allowReads {
            @OptIn(ExperimentalPagingApi::class)
            Pager(
                config = PagingConfig(
                    pageSize = NETWORK_PAGE_SIZE,
                    enablePlaceholders = false
                ),
                remoteMediator = nearbyPlaceRemoteMediator.apply {
                    this.latitude = latitude
                    this.longitude = longitude
                },
                pagingSourceFactory = { dao.select() }
            ).flow
        }
    }

}