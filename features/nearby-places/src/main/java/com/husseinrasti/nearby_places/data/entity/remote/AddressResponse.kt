package com.husseinrasti.nearby_places.data.entity.remote

import com.google.gson.annotations.SerializedName

data class AddressResponse(
    @SerializedName("freeformAddress")
    val address: String?,
)