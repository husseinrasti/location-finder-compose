package com.husseinrasti.nearby_places.data.entity.remote

import com.google.gson.annotations.SerializedName
import com.husseinrasti.core.model.ResponseMapper
import com.husseinrasti.nearby_places.data.entity.dto.PlaceEntity

data class PlaceResponse(
    @SerializedName("id")
    val id: String?,
    @SerializedName("address")
    val address: AddressResponse?,
    @SerializedName("poi")
    val detail: DetailResponse?
) : ResponseMapper<PlaceEntity.Item> {
    override fun toDto(): PlaceEntity.Item {
        return PlaceEntity.Item(
            id = id ?: "Unknown",
            address = address?.address ?: "Unknown",
            name = detail?.name ?: "Unknown",
            phone = detail?.phone ?: "",
            url = detail?.url ?: ""
        )
    }
}