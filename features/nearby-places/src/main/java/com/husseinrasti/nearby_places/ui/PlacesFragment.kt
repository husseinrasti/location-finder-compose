package com.husseinrasti.nearby_places.ui

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Intent
import android.location.Location
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.StringRes
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.findNavController
import androidx.paging.CombinedLoadStates
import androidx.paging.LoadState
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.RecyclerView
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationRequest.PRIORITY_HIGH_ACCURACY
import com.google.android.gms.location.LocationServices
import com.google.android.gms.tasks.CancellationTokenSource
import com.google.android.gms.tasks.Task
import com.husseinrasti.components.*
import com.husseinrasti.components.R
import com.husseinrasti.core.model.toFailure
import com.husseinrasti.core.extensions.*
import com.husseinrasti.core.model.GPS_REQUEST_CODE
import com.husseinrasti.core.model.PERMISSION_REQUEST_CODE
import com.husseinrasti.core.utils.PermissionHelper
import com.husseinrasti.core.R as CoreR
import com.husseinrasti.nearby_places.databinding.PlacesFragmentBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import timber.log.Timber


/**
 * Created by Hussein Rasti on 3/19/22.
 */
@AndroidEntryPoint
class PlacesFragment : Fragment(), PermissionHelper.PermissionCallback {

    private val viewModel: PlaceViewModel by viewModels()

    private lateinit var adapter: PlaceAdapter
    private lateinit var footerStateAdapter: FooterStateAdapter

    private var permissionHelper: PermissionHelper? = null

    private val fusedLocationClient: FusedLocationProviderClient by lazy {
        LocationServices.getFusedLocationProviderClient(requireContext().applicationContext)
    }
    private var cancellationTokenSource = CancellationTokenSource()

    private var loadingDialog: LoadingDialog? = null

    private var _binding: PlacesFragmentBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        onSetupViewModel()
        permissionHelper = PermissionHelper(
            this,
            arrayOf(
                Manifest.permission.ACCESS_FINE_LOCATION,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ),
            PERMISSION_REQUEST_CODE
        )
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = PlacesFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    @SuppressLint("MissingPermission", "StringFormatMatches")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onSetupUI()
    }

    private fun onGetPlaces(latitude: Double, longitude: Double) {
        lifecycleScope.launchWhenStarted {
            viewModel.userIntent.send(
                PlacesIntent.GetPlaces(latitude = latitude, longitude = longitude)
            )
        }
    }

    private fun onPermissionHelper() {
        tryCatch { permissionHelper?.request(this) }
    }

    private fun onSetupUI() {
        adapter = PlaceAdapter { id ->
            findNavController().navigate(
                R.id.placeDetail,
                toBundle(key = "id", value = id)
            )
        }
        footerStateAdapter = FooterStateAdapter(adapter::retry)
        adapter.addLoadStateListener { adapterLoadingErrorHandling(it) }
        binding.recyclerPlaces.run { addItemDecoration(DividerItemDecoration(requireContext(), RecyclerView.VERTICAL)) }
        binding.recyclerPlaces.adapter = adapter.withLoadStateFooter(footer = footerStateAdapter)
        binding.fabCurrentLocation.setOnClickListener { onPermissionHelper() }
    }

    private fun adapterLoadingErrorHandling(combinedLoadStates: CombinedLoadStates) {
        when (combinedLoadStates.refresh) {
            is LoadState.Loading -> {
                onShowLoading()
            }
            is LoadState.NotLoading -> {
                onHideLoading()
            }
            else -> {
                onHideLoading()
                val error = when {
                    combinedLoadStates.prepend is LoadState.Error -> combinedLoadStates.prepend as LoadState.Error
                    combinedLoadStates.source.prepend is LoadState.Error -> combinedLoadStates.prepend as LoadState.Error
                    combinedLoadStates.append is LoadState.Error -> combinedLoadStates.append as LoadState.Error
                    combinedLoadStates.source.append is LoadState.Error -> combinedLoadStates.append as LoadState.Error
                    combinedLoadStates.refresh is LoadState.Error -> combinedLoadStates.refresh as LoadState.Error
                    else -> null
                }
                error?.run {
                    binding.root.snack(error.error.toFailure().message ?: getString(CoreR.string.msg_error)) {}
                }
            }
        }
    }

    private fun onSetupViewModel() {
//        onGetPlaces(latitude = 37.337, longitude = -121.89) //for test
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.uiState.collect { uiState ->
                    when (uiState) {
                        PlacesUiState.Idle -> {
                        }
                        is PlacesUiState.Error -> {
                            if (uiState.type == PlacesUiState.Error.Type.NETWORK) {
                                binding.root.snack(uiState.msg ?: getString(CoreR.string.msg_error)) {}
                            } else {
                                binding.recyclerPlaces.gone()
                                binding.txtError.visible()
                            }
                        }
                        is PlacesUiState.Places -> {
                            adapter.submitData(uiState.data)
                        }
                        is PlacesUiState.LastLocation -> {
                            uiState.latLng?.let {
                                binding.txtError.gone()
                                onGetPlaces(latitude = it.latitude, longitude = it.longitude)
                            } ?: run {
                                binding.recyclerPlaces.gone()
                                binding.txtError.visible()
                            }
                        }
                    }
                }
            }
        }
    }

    @SuppressLint("MissingPermission", "StringFormatMatches")
    private fun onCurrentPlace() {
        if (!isGps()) {
            onEnableLocationDialog()
        } else if (hasPermission()) {
            binding.fabCurrentLocation.blinking()
            val currentLocationTask: Task<Location> = fusedLocationClient.getCurrentLocation(
                PRIORITY_HIGH_ACCURACY,
                cancellationTokenSource.token
            )
            currentLocationTask.addOnCompleteListener { task: Task<Location> ->
                tryCatch {
                    binding.fabCurrentLocation.bounce()
                    if (task.isSuccessful) {
                        val place: Location = task.result
                        viewModel.shouldRefresh(place) {
                            onGetPlaces(place.latitude, place.longitude)
                            adapter.refresh()
                            binding.root.snack(
                                getString(R.string.str_current_location, place.latitude, place.longitude)
                            ) {}
                        }
                    } else binding.root.snack(R.string.msg_not_found_current_location) {}
                }
            }
        }
    }

    @Deprecated("Deprecated in Java")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        when (requestCode) {
            GPS_REQUEST_CODE -> when (resultCode) {
                Activity.RESULT_OK -> onPermissionHelper()
            }
        }
    }

    @Deprecated("Deprecated in Java")
    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<String>, grantResults: IntArray) {
        permissionHelper?.onRequestPermissionsResult(requestCode, permissions, grantResults)
    }

    override fun onPermissionGranted() {
        onCurrentPlace()
    }

    override fun onIndividualPermissionGranted(grantedPermission: Array<String>) {
        showSnack(R.string.msg_permission_location) { onPermissionHelper() }
    }

    override fun onPermissionDenied() {
        showSnack(R.string.msg_permission_location) { onPermissionHelper() }
    }

    override fun onPermissionDeniedBySystem() {
        showSnack(R.string.msg_setting_activity_permission) { permissionHelper?.openAppDetailsActivity() }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

    private fun hasPermission(): Boolean {
        return permissionHelper?.hasPermission() ?: false
    }

    private fun showSnack(@StringRes messageRes: Int, onAction: () -> Unit) {
        tryCatch { binding.root.snack(messageRes) { action(R.string.btn_allow) { onAction() } } }
    }

    private fun onShowLoading() {
        tryCatch {
            parentFragmentManager.let parentFragmentManager@{ fragmentManager ->
                loadingDialog?.let { return@parentFragmentManager }
                loadingDialog = LoadingDialog().also {
                    if (it.dialog != null && it.dialog!!.isShowing && !it.isRemoving) return@also
                    it.show(fragmentManager)
                }
            }
            binding.txtError.gone()
            binding.recyclerPlaces.gone()
        }
    }

    private fun onHideLoading() {
        tryCatch {
            loadingDialog?.let {
                it.dismiss()
                loadingDialog = null
            }
            binding.recyclerPlaces.visible()
        }
    }

}