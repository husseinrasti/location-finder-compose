package com.husseinrasti.nearby_places.ui

import android.location.Location
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.google.android.gms.maps.model.LatLng
import com.husseinrasti.core.extensions.calculateDistanceTwoLatLng
import com.husseinrasti.core.utils.Logger
import com.husseinrasti.domain.settings.usecase.GetLastLocationUseCase
import com.husseinrasti.nearby_places.data.entity.dto.PlaceEntity
import com.husseinrasti.nearby_places.data.repository.PlaceRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject


/**
 * Created by Hussein Rasti on 3/19/22.
 */
@HiltViewModel
class PlaceViewModel @Inject constructor(
    private val placeRepository: PlaceRepository,
    private val getLastLocationUseCase: GetLastLocationUseCase
) : ViewModel() {

    val userIntent = Channel<PlacesIntent>(Channel.UNLIMITED)

    private var lastLocation: LatLng? = null

    private val _uiState = MutableStateFlow<PlacesUiState>(PlacesUiState.Idle)
    val uiState = _uiState.asStateFlow()

    init {
        handleIntent()
        getLastLocation()
    }

    fun shouldRefresh(currentLocation: Location, onRefresh: () -> Unit) {
        lastLocation?.let {
            if (
                calculateDistanceTwoLatLng(
                    it,
                    LatLng(currentLocation.latitude, currentLocation.longitude)
                ) > 100
            ) onRefresh()
        } ?: onRefresh()
    }

    private fun getLastLocation() {
        viewModelScope.launch {
            getLastLocationUseCase(GetLastLocationUseCase.Params(""))
                .catch {
                    _uiState.emit(PlacesUiState.Error(msg = it.message, PlacesUiState.Error.Type.LOCATION))
                }.collect {
                    Logger.i("Last location: $it")
                    lastLocation = it
                    _uiState.emit(PlacesUiState.LastLocation(it))
                }
        }
    }

    private fun handleIntent() {
        viewModelScope.launch {
            userIntent.consumeAsFlow().collect { intent ->
                Logger.i("Places handleIntent: $intent")
                when (intent) {
                    is PlacesIntent.GetPlaces -> getPlaces(
                        latitude = intent.latitude, longitude = intent.longitude
                    )
                }
            }
        }
    }

    private fun getPlaces(latitude: Double, longitude: Double) {
        viewModelScope.launch {
            placeRepository.getNearbyPlaces(latitude = latitude, longitude = longitude)
                .cachedIn(viewModelScope)
                .distinctUntilChanged()
                .catch {
                    _uiState.emit(PlacesUiState.Error(msg = it.message, PlacesUiState.Error.Type.NETWORK))
                }.collect {
                    _uiState.emit(PlacesUiState.Places(it))
                }
        }
    }

}


sealed interface PlacesIntent {
    data class GetPlaces(val latitude: Double, val longitude: Double) : PlacesIntent
}


sealed interface PlacesUiState {
    data class Places(val data: PagingData<PlaceEntity.Item>) : PlacesUiState

    data class Error(val msg: String?, val type: Type) : PlacesUiState {
        enum class Type {
            NETWORK,
            LOCATION
        }
    }

    data class LastLocation(val latLng: LatLng?) : PlacesUiState

    object Idle : PlacesUiState
}