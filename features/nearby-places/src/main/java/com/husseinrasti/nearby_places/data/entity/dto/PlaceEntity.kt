package com.husseinrasti.nearby_places.data.entity.dto

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

class PlaceEntity {

    @Entity(tableName = "tbl_places")
    data class Item(
        @PrimaryKey(autoGenerate = false)
        @ColumnInfo(name = "id")
        val id: String,
        @ColumnInfo(name = "address")
        val address: String,
        @ColumnInfo(name = "name")
        val name: String,
        @ColumnInfo(name = "url")
        val url: String,
        @ColumnInfo(name = "phone")
        val phone: String
    )

}