package com.husseinrasti.nearby_places.data.remote

import com.husseinrasti.core.network.BuildConfig
import com.husseinrasti.core.network.routes.SearchRoute
import com.husseinrasti.nearby_places.data.entity.remote.ResultResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query


/**
 * Created by Hussein Rasti.
 */
interface NearbyPlaceApi {

    @GET(SearchRoute.nearbySearch)
    suspend fun getNearbyPlaces(
        @Query("lat") latitude: Double,
        @Query("lon") longitude: Double,
        @Query("limit") limit: Int,
        @Query("ofs") offset: Int,
        @Query("key") key: String = BuildConfig.API_KEY
    ): Response<ResultResponse>

}