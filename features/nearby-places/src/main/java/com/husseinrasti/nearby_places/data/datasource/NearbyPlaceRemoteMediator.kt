package com.husseinrasti.nearby_places.data.datasource

import android.content.res.Resources
import androidx.paging.ExperimentalPagingApi
import androidx.paging.LoadType
import androidx.paging.PagingState
import androidx.paging.RemoteMediator
import com.google.android.gms.maps.model.LatLng
import com.husseinrasti.core.R
import com.husseinrasti.core.extensions.calculateDistanceTwoLatLng
import com.husseinrasti.core.model.STARTING_PAGE_INDEX
import com.husseinrasti.core.model.toFailure
import com.husseinrasti.core.network.callback.errorResponse
import com.husseinrasti.core.utils.Logger
import com.husseinrasti.domain.remotekeys.entity.RemoteKeysEntity
import com.husseinrasti.domain.remotekeys.usecase.DeleteRemoteKeysUseCase
import com.husseinrasti.domain.remotekeys.usecase.FetchKeysByTypeUseCase
import com.husseinrasti.domain.remotekeys.usecase.InsertRemoteKeysUseCase
import com.husseinrasti.domain.settings.usecase.GetLastLocationUseCase
import com.husseinrasti.domain.settings.usecase.InsertLocationUseCase
import com.husseinrasti.nearby_places.data.dao.PlaceDao
import com.husseinrasti.nearby_places.data.entity.dto.PlaceEntity
import com.husseinrasti.nearby_places.data.remote.NearbyPlaceApi
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.flow.first
import kotlinx.coroutines.withContext
import javax.inject.Inject


const val TYPE_REMOTE_KEYS = "NearbyPlaces"


/**
 * Created by Hussein Rasti.
 */
@OptIn(ExperimentalPagingApi::class)
class NearbyPlaceRemoteMediator @Inject constructor(
    private val api: NearbyPlaceApi,
    private val dao: PlaceDao,
    private val resources: Resources,
    private val fetchKeysByTypeUseCase: FetchKeysByTypeUseCase,
    private val insertRemoteKeysUseCase: InsertRemoteKeysUseCase,
    private val deleteRemoteKeysUseCase: DeleteRemoteKeysUseCase,
    private val getLastLocationUseCase: GetLastLocationUseCase,
    private val insertLocationUseCase: InsertLocationUseCase
) : RemoteMediator<Int, PlaceEntity.Item>() {

    private var lastOffset = 0 //Latest offset than items fetched from network.
    private var totalResult = 0 //Latest total of results that should be fetched from network.

    var latitude: Double? = null
    var longitude: Double? = null

    override suspend fun initialize(): InitializeAction {
        return if (shouldFetchFromLocal()) {
            // Cached data is up-to-date, so there is no need to re-fetch from the network.
            InitializeAction.SKIP_INITIAL_REFRESH
        } else {
            // Need to refresh cached data from network;
            // returning LAUNCH_INITIAL_REFRESH here will also block RemoteMediator's
            // APPEND and PREPEND from running until REFRESH succeeds.
            InitializeAction.LAUNCH_INITIAL_REFRESH
        }
    }

    override suspend fun load(
        loadType: LoadType,
        state: PagingState<Int, PlaceEntity.Item>
    ): MediatorResult {
        Logger.i("Load type paging: ${loadType.name}")
        if (loadType == LoadType.REFRESH) {
            deleteRemoteKeysUseCase(RemoteKeysEntity.Item(type = TYPE_REMOTE_KEYS))
        }
        val page = when (loadType) {
            LoadType.REFRESH -> {
                val remoteKeys = getRemoteKeyClosestToCurrentPosition(state)
                remoteKeys?.offset?.let { lastOffset = it }
                remoteKeys?.total?.let { totalResult = it }
                remoteKeys?.nextKey?.minus(1) ?: STARTING_PAGE_INDEX
            }
            LoadType.PREPEND -> {
                val remoteKeys = getRemoteKeyForFirstItem(state)
                remoteKeys?.offset?.let { lastOffset = it }
                remoteKeys?.total?.let { totalResult = it }
                // If remoteKeys is null, that means the refresh result is not in the database yet.
                remoteKeys?.prevKey
                    ?: return MediatorResult.Success(endOfPaginationReached = remoteKeys != null)
            }
            LoadType.APPEND -> {
                val remoteKeys = getRemoteKeyForLastItem(state)
                remoteKeys?.offset?.let { lastOffset = it }
                remoteKeys?.total?.let { totalResult = it }
                remoteKeys?.nextKey
                    ?: return MediatorResult.Success(endOfPaginationReached = remoteKeys != null)
            }
        }
        //The offset is obtained by multiplying the page number by the number of items on the page.
        val offset = page * state.config.pageSize
        Logger.i("Offset: $offset , last: $lastOffset , total: $totalResult")
        return if (offset < lastOffset && loadType != LoadType.REFRESH) {
            MediatorResult.Success(endOfPaginationReached = totalResult <= offset)
        } else try {
            val response = api.getNearbyPlaces(
                latitude = latitude!!,
                longitude = longitude!!,
                offset = offset,
                limit = state.config.pageSize
            )
            if (response.isSuccessful) {
                val total = response.body()?.summary?.totalResults ?: Int.MAX_VALUE
                val data = response.body()?.results!!
                val endOfPaginationReached = data.isEmpty() || offset >= total
                val prevKey = if (page == STARTING_PAGE_INDEX) null else page - 1
                val nextKey = if (endOfPaginationReached) null else page + 1
                insertRemoteKeysUseCase(
                    RemoteKeysEntity.Item(
                        type = TYPE_REMOTE_KEYS,
                        prevKey = prevKey,
                        nextKey = nextKey,
                        total = total,
                        offset = offset
                    )
                )
                insertLocationUseCase(
                    InsertLocationUseCase.Params(LatLng(latitude!!, longitude!!))
                )
                shouldClearTable(loadType)
                dao.insert(data.map { it.toDto() })
                MediatorResult.Success(endOfPaginationReached = endOfPaginationReached)
            } else {
                MediatorResult.Error(response.errorResponse(resources.getString(R.string.msg_error)))
            }
        } catch (e: Exception) {
            e.printStackTrace()
            MediatorResult.Error(e.toFailure(resources.getString(R.string.msg_error)))
        }
    }

    private suspend fun shouldFetchFromLocal(): Boolean {
        return try {
            //Get last location from settings
            val lastLocation = getLastLocationUseCase.invoke(GetLastLocationUseCase.Params("")).first()
            //Calculate the distance between two locations and check them to be more than 100m
            val distance = withContext(Dispatchers.IO) {
                calculateDistanceTwoLatLng(lastLocation!!, LatLng(latitude!!, longitude!!))
            }
            //Get count of places on the database and check them to be more than 0
            val countPlaces = withContext(Dispatchers.IO) { dao.count() }
            Logger.i(
                """
                    Distance between two points is $distance ,
                    count= $countPlaces ,
                    condition=${(distance <= 100) && countPlaces > 0} 
                    """
            )
            (distance <= 100) && countPlaces > 0
        } catch (e: Exception) {
            e.printStackTrace()
            false
        }
    }

    private suspend fun shouldClearTable(loadType: LoadType) {
        if (loadType == LoadType.REFRESH) dao.clear()
    }

    private suspend fun getRemoteKeyForLastItem(
        state: PagingState<Int, PlaceEntity.Item>
    ): RemoteKeysEntity.Item? {
        // Get the last page that was retrieved, that contained items.
        // From that last page, get the last item
        return state.pages.lastOrNull { it.data.isNotEmpty() }?.data?.lastOrNull()
            ?.let {
                // Get the remote keys of the last item retrieved
                fetchKeysByTypeUseCase(FetchKeysByTypeUseCase.Params(TYPE_REMOTE_KEYS))
            }
    }

    private suspend fun getRemoteKeyForFirstItem(
        state: PagingState<Int, PlaceEntity.Item>
    ): RemoteKeysEntity.Item? {
        // Get the first page that was retrieved, that contained items.
        // From that first page, get the first item
        return state.pages.firstOrNull { it.data.isNotEmpty() }?.data?.firstOrNull()
            ?.let {
                // Get the remote keys of the first items retrieved
                fetchKeysByTypeUseCase(FetchKeysByTypeUseCase.Params(TYPE_REMOTE_KEYS))
            }
    }

    private suspend fun getRemoteKeyClosestToCurrentPosition(
        state: PagingState<Int, PlaceEntity.Item>
    ): RemoteKeysEntity.Item? {
        // The paging library is trying to load data after the anchor position
        // Get the item closest to the anchor position
        return state.anchorPosition?.let { position ->
            state.closestItemToPosition(position)?.let {
                fetchKeysByTypeUseCase(FetchKeysByTypeUseCase.Params(TYPE_REMOTE_KEYS))
            }
        }
    }

}