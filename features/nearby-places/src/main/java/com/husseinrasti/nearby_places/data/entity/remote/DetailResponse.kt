package com.husseinrasti.nearby_places.data.entity.remote

import com.google.gson.annotations.SerializedName

data class DetailResponse(
    @SerializedName("name")
    val name: String?,
    @SerializedName("phone")
    val phone: String?,
    @SerializedName("url")
    val url: String?
)