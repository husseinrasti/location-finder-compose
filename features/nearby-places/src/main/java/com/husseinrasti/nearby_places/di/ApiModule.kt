package com.husseinrasti.nearby_places.di

import com.husseinrasti.nearby_places.data.remote.NearbyPlaceApi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

/**
 * Created by Hussein Rasti on 5/27/22.
 */
@Module
@InstallIn(SingletonComponent::class)
class ApiModule {

    @Provides
    @Singleton
    fun provideNearbyPlaceApi(retrofit: Retrofit): NearbyPlaceApi {
        return retrofit.create(NearbyPlaceApi::class.java)
    }

}