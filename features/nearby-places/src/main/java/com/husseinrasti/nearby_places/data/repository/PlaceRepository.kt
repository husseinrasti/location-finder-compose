package com.husseinrasti.nearby_places.data.repository

import androidx.paging.PagingData
import com.husseinrasti.nearby_places.data.entity.dto.PlaceEntity
import kotlinx.coroutines.flow.Flow


/**
 * Created by Hussein Rasti.
 */
interface PlaceRepository {

    fun getNearbyPlaces(latitude: Double, longitude: Double): Flow<PagingData<PlaceEntity.Item>>

}