package com.husseinrasti.place_detail.data.entity.remote

import com.google.gson.annotations.SerializedName
import com.husseinrasti.core.model.ResponseMapper
import com.husseinrasti.place_detail.data.entity.dto.PlaceDetailEntity

data class ResultResponse(
    @SerializedName("results")
    val results: List<PlaceResponse>?
) : ResponseMapper<PlaceDetailEntity.Item> {
    override fun toDto(): PlaceDetailEntity.Item {
        val item = results!!.first()
        return PlaceDetailEntity.Item(
            id = item.id ?: "Unknown",
            address = item.address?.address ?: "Unknown",
            country = item.address?.country ?: "",
            local = item.address?.local ?: "",
            postalCode = item.address?.postalCode ?: "",
            name = item.detail?.name ?: "Unknown",
            phone = item.detail?.phone ?: "",
            url = item.detail?.url ?: ""
        )
    }
}