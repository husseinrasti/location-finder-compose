package com.husseinrasti.place_detail.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.navigation.fragment.navArgs
import com.husseinrasti.components.*
import com.husseinrasti.core.extensions.*
import com.husseinrasti.place_detail.data.entity.dto.PlaceDetailEntity
import com.husseinrasti.core.R as CoreR
import com.husseinrasti.place_detail.databinding.PlaceDetailFragmentBinding
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch


/**
 * Created by Hussein Rasti on 3/19/22.
 */
@AndroidEntryPoint
class PlaceDetailFragment : Fragment() {

    private val args: PlaceDetailFragmentArgs by navArgs()

    private val viewModel: PlaceDetailViewModel by viewModels()

    private var loadingDialog: LoadingDialog? = null

    private var _binding: PlaceDetailFragmentBinding? = null
    private val binding get() = _binding!!

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        onSetupViewModel()
        onGetPlaceById(args.id)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = PlaceDetailFragmentBinding.inflate(inflater, container, false)
        return binding.root
    }

    private fun onGetPlaceById(id: String) {
        lifecycleScope.launchWhenStarted {
            viewModel.userIntent.send(PlaceDetailIntent.GetPlaceDetail(id = id))
        }
    }

    private fun onSetupViewModel() {
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                viewModel.uiState.collect { uiState ->
                    when (uiState) {
                        PlaceDetailUiState.Loading -> {
                            onShowLoading()
                        }
                        is PlaceDetailUiState.Error -> {
                            onHideLoading()
                            binding.root.snack(uiState.msg ?: getString(CoreR.string.msg_error)) {}
                        }
                        is PlaceDetailUiState.Places -> {
                            onHideLoading()
                            onUpdateUI(uiState.data)
                        }
                    }
                }
            }
        }
    }

    private fun onUpdateUI(data: PlaceDetailEntity.Item) {
        binding.name.text = data.name
        binding.address.text = data.address
        binding.phone.text = getString(R.string.str_phone, data.phone)
        binding.page.text = getString(R.string.str_website, data.url)
        binding.country.text = getString(R.string.str_country, data.country)
        binding.province.text = getString(R.string.str_province, data.local)
        binding.postalCode.text = getString(R.string.str_postal_code, data.postalCode)
    }

    private fun onShowLoading() {
        tryCatch {
            parentFragmentManager.let parentFragmentManager@{ fragmentManager ->
                loadingDialog?.let { return@parentFragmentManager }
                loadingDialog = LoadingDialog().also {
                    if (it.dialog != null && it.dialog!!.isShowing && !it.isRemoving) return@also
                    it.show(fragmentManager)
                }
            }
        }
    }

    private fun onHideLoading() {
        tryCatch {
            loadingDialog?.let {
                it.dismiss()
                loadingDialog = null
            }
        }
    }

}