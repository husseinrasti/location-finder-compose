package com.husseinrasti.place_detail.data.entity.dto


class PlaceDetailEntity {

    data class Item(
        val id: String,
        val country: String,
        val address: String,
        val local: String,
        val postalCode: String,
        val name: String,
        val url: String,
        val phone: String
    )

}