package com.husseinrasti.place_detail.data.entity.remote

import com.google.gson.annotations.SerializedName

data class AddressResponse(
    @SerializedName("country")
    val country: String?,
    @SerializedName("freeformAddress")
    val address: String?,
    @SerializedName("localName")
    val local: String?,
    @SerializedName("postalCode")
    val postalCode: String?
)