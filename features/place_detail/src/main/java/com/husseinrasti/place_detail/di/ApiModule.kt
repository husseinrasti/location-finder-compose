package com.husseinrasti.place_detail.di

import com.husseinrasti.place_detail.data.remote.PlaceDetailApi
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

/**
 * Created by Hussein Rasti on 5/27/22.
 */
@Module
@InstallIn(SingletonComponent::class)
class ApiModule {

    @Provides
    @Singleton
    fun providePlaceDetailApi(retrofit: Retrofit): PlaceDetailApi {
        return retrofit.create(PlaceDetailApi::class.java)
    }

}