package com.husseinrasti.place_detail.data.repository

import android.content.res.Resources
import com.husseinrasti.core.network.callback.fetch
import com.husseinrasti.place_detail.data.entity.dto.PlaceDetailEntity
import com.husseinrasti.place_detail.data.remote.PlaceDetailApi
import kotlinx.coroutines.Dispatchers
import javax.inject.Inject


/**
 * Created by Hussein Rasti.
 */
class PlaceDetailRepositoryImpl @Inject constructor(
    private val resources: Resources,
    private val api: PlaceDetailApi
) : PlaceDetailRepository {

    override suspend fun getDetailById(id: String): Result<PlaceDetailEntity.Item> {
        return fetch(
            dispatcher = Dispatchers.IO,
            resources = resources,
            fetch = { api.getPlaceById(id) }
        )
    }

}