package com.husseinrasti.place_detail.data.entity.remote

import com.google.gson.annotations.SerializedName

data class PlaceResponse(
    @SerializedName("id")
    val id: String?,
    @SerializedName("address")
    val address: AddressResponse?,
    @SerializedName("poi")
    val detail: DetailResponse?
)