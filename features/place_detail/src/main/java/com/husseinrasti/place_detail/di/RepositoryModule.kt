package com.husseinrasti.place_detail.di

import android.content.res.Resources
import com.husseinrasti.place_detail.data.remote.PlaceDetailApi
import com.husseinrasti.place_detail.data.repository.PlaceDetailRepository
import com.husseinrasti.place_detail.data.repository.PlaceDetailRepositoryImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped


/**
 * Created by Hussein Rasti on 5/27/22.
 */

@Module
@InstallIn(ViewModelComponent::class)
class RepositoryModule {

    @Provides
    @ViewModelScoped
    fun providePlaceDetailRepository(
        resources: Resources,
        api: PlaceDetailApi
    ): PlaceDetailRepository = PlaceDetailRepositoryImpl(
        resources = resources,
        api = api
    )

}