package com.husseinrasti.place_detail.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.husseinrasti.core.utils.Logger
import com.husseinrasti.place_detail.data.entity.dto.PlaceDetailEntity
import com.husseinrasti.place_detail.data.repository.PlaceDetailRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.channels.Channel
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import javax.inject.Inject


/**
 * Created by Hussein Rasti on 3/19/22.
 */
@HiltViewModel
class PlaceDetailViewModel @Inject constructor(
    private val repository: PlaceDetailRepository
) : ViewModel() {

    val userIntent = Channel<PlaceDetailIntent>(Channel.UNLIMITED)

    private val _uiState = MutableStateFlow<PlaceDetailUiState>(PlaceDetailUiState.Loading)
    val uiState = _uiState.asStateFlow()

    init {
        handleIntent()
    }

    private fun handleIntent() {
        viewModelScope.launch {
            userIntent.consumeAsFlow().collect { intent ->
                Logger.i("Places handleIntent: $intent")
                when (intent) {
                    is PlaceDetailIntent.GetPlaceDetail -> getPlaceDetail(id = intent.id)
                }
            }
        }
    }

    private fun getPlaceDetail(id: String) {
        viewModelScope.launch {
            repository.getDetailById(id = id).fold(
                onSuccess = {
                    _uiState.emit(PlaceDetailUiState.Places(it))
                },
                onFailure = {
                    _uiState.emit(PlaceDetailUiState.Error(msg = it.message))
                }
            )
        }
    }

}


sealed interface PlaceDetailIntent {
    data class GetPlaceDetail(val id: String) : PlaceDetailIntent
}


sealed interface PlaceDetailUiState {
    data class Places(val data: PlaceDetailEntity.Item) : PlaceDetailUiState
    data class Error(val msg: String?) : PlaceDetailUiState
    object Loading : PlaceDetailUiState
}