package com.husseinrasti.place_detail.data.remote

import com.husseinrasti.core.network.BuildConfig
import com.husseinrasti.core.network.routes.SearchRoute
import com.husseinrasti.place_detail.data.entity.remote.ResultResponse
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Query


/**
 * Created by Hussein Rasti.
 */
interface PlaceDetailApi {

    @GET(SearchRoute.placeById)
    suspend fun getPlaceById(
        @Query("entityId") id: String,
        @Query("key") key: String = BuildConfig.API_KEY
    ): Response<ResultResponse>

}