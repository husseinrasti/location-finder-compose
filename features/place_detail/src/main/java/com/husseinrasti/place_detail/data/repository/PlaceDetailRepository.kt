package com.husseinrasti.place_detail.data.repository

import com.husseinrasti.place_detail.data.entity.dto.PlaceDetailEntity


/**
 * Created by Hussein Rasti.
 */
interface PlaceDetailRepository {

    suspend fun getDetailById(id: String): Result<PlaceDetailEntity.Item>

}