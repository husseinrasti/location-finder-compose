package com.husseinrasti.domain.settings.usecase

import com.google.android.gms.maps.model.LatLng
import com.husseinrasti.core.usecase.FlowUseCase
import com.husseinrasti.domain.settings.repository.SettingsRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject


class GetLastLocationUseCase @Inject constructor(
    private val repository: SettingsRepository
) : FlowUseCase<GetLastLocationUseCase.Params, LatLng?> {

    override suspend fun invoke(params: Params): Flow<LatLng?> {
        return repository.getLastLocation()
    }

    @JvmInline
    value class Params(val nothing: Any)
}