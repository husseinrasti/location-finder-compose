package com.husseinrasti.domain.settings.repository

import com.google.android.gms.maps.model.LatLng
import kotlinx.coroutines.flow.Flow


/**
 * Created by Hussein Rasti.
 */
interface SettingsRepository {

    fun getLastLocation(): Flow<LatLng?>

    suspend fun insertLocation(latLng: LatLng)

    suspend fun clear()

}