package com.husseinrasti.domain.settings.usecase

import com.google.android.gms.maps.model.LatLng
import com.husseinrasti.core.usecase.CompletableUseCase
import com.husseinrasti.domain.settings.repository.SettingsRepository
import javax.inject.Inject


class InsertLocationUseCase @Inject constructor(
    private val repository: SettingsRepository
) : CompletableUseCase<InsertLocationUseCase.Params> {

    override suspend fun invoke(params: Params) {
        repository.insertLocation(params.latLng)
    }

    @JvmInline
    value class Params(val latLng: LatLng)
}