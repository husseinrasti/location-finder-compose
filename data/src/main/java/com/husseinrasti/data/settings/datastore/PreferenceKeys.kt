package com.husseinrasti.data.settings.datastore

import androidx.datastore.preferences.core.stringPreferencesKey

object PreferenceKeys {
    val KEY_LOCATION = stringPreferencesKey("location")
}