package com.husseinrasti.data.settings.repository

import com.google.android.gms.maps.model.LatLng
import com.husseinrasti.data.settings.datastore.SettingsDataStore
import com.husseinrasti.domain.settings.repository.SettingsRepository
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject


/**
 * Created by Hussein Rasti.
 */
class SettingsRepositoryImpl @Inject constructor(
    private val settingsDataStore: SettingsDataStore
) : SettingsRepository {

    override fun getLastLocation(): Flow<LatLng?> {
        return settingsDataStore.location
    }

    override suspend fun insertLocation(latLng: LatLng) {
        settingsDataStore.insertLocation(latLng)
    }

    override suspend fun clear() {
        settingsDataStore.clear()
    }

}