package com.husseinrasti.data.settings.datastore

import androidx.datastore.core.DataStore
import androidx.datastore.preferences.core.Preferences
import androidx.datastore.preferences.core.edit
import com.google.android.gms.maps.model.LatLng
import com.husseinrasti.core.extensions.toJson
import com.husseinrasti.core.extensions.toLatLng
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.map
import javax.inject.Inject

class SettingsDataStore @Inject constructor(private val dataStore: DataStore<Preferences>) {

    val location: Flow<LatLng?> = dataStore.data.map { preferences ->
        preferences[PreferenceKeys.KEY_LOCATION]?.toLatLng()
    }

    suspend fun insertLocation(latLng: LatLng) {
        dataStore.edit { preferences ->
            preferences[PreferenceKeys.KEY_LOCATION] = latLng.toJson()
        }
    }

    suspend fun clear() {
        dataStore.edit { preferences -> preferences.clear() }
    }

}