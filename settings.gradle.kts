pluginManagement {
    repositories {
        gradlePluginPortal()
        google()
        mavenCentral()
    }
}
dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        google()
        mavenCentral()
        gradlePluginPortal()
    }
}
rootProject.name = "SampleLocationFinder"

includeBuild("build-logic")

include(":app")
include(":core")
include(":core-network")
include(":core-model")
include(":data")
include(":domain")
include(":components")
include(":features:nearby-places")
include(":features:place_detail")
